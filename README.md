# Personal Site

This is my personal site. It is currently hosted in AWS using
ElasticBeanstalk. To deploy and test locally, use docker (see Dockerfile).

Continuous Integration has been setup so that after every push to
master, code is deployed in the AWS instance. Deployment is done
using gitlab-ci capabilities and the `dpl` tool.

