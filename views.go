package main

import (
	"net/http"
	"log"
	"html/template"
	"io/ioutil"
	"errors"
)

type GenericView struct {
	Template string
	Data     interface{}
}

func NewProjectsView() (GenericView) {
	file, err := ioutil.ReadFile("./projects.json")
	if err != nil {
		log.Fatalln(err)
	}

	projects, err := ReadAllProjects(file)
	if err != nil {
		log.Fatalln(err)
	}

	data := dataWithTitle("Projects")
	data["Projects"] = projects

	return GenericView{
		"website/projects.html",
		data,
	}
}

func NewResumeView() GenericView {
	file, err := ioutil.ReadFile("./experience.json")
	if err != nil {
		log.Fatalln(err)
	}

	experiences, err := ReadExperience(file)
	if err != nil {
		log.Fatalln(err)
	}

	data := dataWithTitle("Resume")
	data["Resume"] = *experiences

	return GenericView{
		"website/resume.html",
		data,
	}
}

func NewHomeView() GenericView {
	data := dataWithTitle("Home")
	return GenericView{
		"website/home.html",
		data,
	}
}


func (v GenericView) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	err := renderTemplate(v, w)
	if err != nil {
		log.Fatalln(err)
	}
}

func renderTemplate(view GenericView, w http.ResponseWriter) error {
	t := readTemplate(view.Template)
	if t == nil {
		return errors.New("Couldn't read template")
	}
	return t.ExecuteTemplate(w, "base.html", view.Data)
}

func readTemplate(templateName string) (*template.Template) {
	t, err := template.ParseFiles("website/base.html", templateName)
	if err != nil {
		log.Fatal(err)
		return nil
	}

	return t
}

func dataWithTitle(title string) map[string]interface{} {
	data := make(map[string]interface{})
	data["Title"] = title

	return data
}
