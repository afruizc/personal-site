FROM golang:1.9

ENV SRC_DIR=/go/src/gitlab.com/afruizc/personal_site/
RUN mkdir -p $SRC_DIR
ADD . $SRC_DIR
WORKDIR $SRC_DIR
RUN go install gitlab.com/afruizc/personal_site
ADD ./projects.json /go/bin/
ADD ./experience.json /go/bin/
ENTRYPOINT /go/bin/personal_site
EXPOSE 5000
