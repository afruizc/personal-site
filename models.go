package main

import "encoding/json"

type Link struct {
	Label string
	Url string
}

type Project struct {
	Title string
	Body string
	Overview string
	MainFeatures []string
	Technology string
	Technologies []string
	ExternalLinks []Link
}

type Job struct {
	Title, StartDate, EndDate, Contribution string
}

type Education struct {
	Name, Program, GraduationDate, Gpa string
}

type Experience struct {
	Jobs []Job
	Educations []Education
}

func ReadAllProjects(data []byte) ([]Project, error) {
	var projects []Project
	err := json.Unmarshal(data, &projects)
	if err != nil {
		return nil, err
	}

	return projects, nil
}

func ReadExperience(data []byte) (*Experience, error) {
	var experience Experience
	err := json.Unmarshal(data, &experience)
	if err != nil {
		return nil, err
	}

	return &experience, nil
}
