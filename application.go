package main

import (
	"net/http"
	"os"
	"fmt"
)

const (
	//SITE_FOLDER = "./website"
	INTERNAL_ERROR_STATUS = http.StatusInternalServerError
)

var INTERNAL_ERROR = http.StatusText(INTERNAL_ERROR_STATUS)

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "5000"
	}
	fmt.Printf("Starting in port: %s\n", port)

	staticFileHandler := http.FileServer(http.Dir("website/static/"))
	staticFileHandler = http.StripPrefix("/static/", staticFileHandler)

	http.Handle("/", NewHomeView())
	http.Handle("/projects/", NewProjectsView())
	http.Handle("/resume/", NewResumeView())
	http.Handle("/static/", staticFileHandler)

	http.ListenAndServe(":" + port, nil)
}

